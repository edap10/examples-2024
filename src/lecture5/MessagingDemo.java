package lecture5;

import java.util.Scanner;

import actor.ActorThread;

public class MessagingDemo {

    private ClientThread    ct = new ClientThread();
    private FibonacciThread ft = new FibonacciThread();

    class ClientThread extends ActorThread<String> {

        @Override
        public void run() {
            try {
                Scanner scan = new Scanner(System.in);
                while (true) {
                    System.out.println("ClientThread: awaiting your order.");

                    int n = scan.nextInt();

                    if (n < 1) {
                        ft.interrupt();
                        return;
                    }

                    ft.send(n);

                    String resp = receive();
                    System.out.println("ClientThread got '" + resp + "'");
                }
            } catch (InterruptedException e) {
                throw new Error(e);
            }
        }
    }

    class FibonacciThread extends ActorThread<Integer> {

        private int fib(int n) throws InterruptedException {
            int f2 = 0;
            int f1 = 1;
            for (int k = 2; k <= n; k++) {
                int s = f2 + f1;
                f2 = f1;
                f1 = s;
                Thread.sleep(100);
            }
            return f1;
        }

        @Override
        public void run() {
            try {
                while (true) {
                    System.out.println("FibonacciThread: awaiting message.");

                    int n = receive();

                    System.out.println("FibonacciThread: n = " + n);

                    int result = fib(n);

                    String response = "FibonacciThread: result = " + result;
                    ct.send(response);
                }
            } catch (InterruptedException e) {
                System.out.println("FibonacciThread: bye!");
            }
        }
    }

    public void demo() {
        ct.start();
        ft.start();
    }

    public static void main(String[] args) throws InterruptedException {
        var app = new MessagingDemo();
        app.demo();
    }
}
