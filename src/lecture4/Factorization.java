package lecture4;

import java.math.BigInteger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import lecture4.factorization.RSA;

public class Factorization {

    public static void main(String[] args) throws Exception {
        BigInteger n = new BigInteger("108052586295536081");
        String code = "9isugpbqckalpyrz3f7etse1dq451b5k8exgcnqlyl71b7nnn4nyzj9tamnqwcoqk3gpbm38c6dco8vo38ugpjk1bun8y94108mvc102cewf0c2w2r49rxrd7byazqjq7spf0ivadwfku6w5ylwq4bbykq4v7vzr6udfwrec6kjhw6k";

        ExecutorService pool = Executors.newFixedThreadPool(4);

        Future<String> f = pool.submit(() ->  {
            String result = RSA.crack(code, n);
            return result;
        });

        for (int i = 0; i < 8; i++) {
            System.out.println("do be doo");
            Thread.sleep(1000);
        }

        System.out.println("klar:");

        String s = f.get();
        System.out.println("result:\n'" + s + "'");
    }
}
