package lecture4.factorization;

import java.math.BigInteger;

import rsa.ProgressTracker;

public class RSA {

    /** Simple wrapper for the RSA factorization code from lab 4. */
    public static String crack(String code, BigInteger n) {
        try {
            ProgressTracker nevermind = x -> { /* a ProgressTracker that does nothing */ };
            return rsa.Factorizer.crack(code, n, nevermind);
        } catch (InterruptedException unexpected) {
            throw new Error(unexpected);
        }
    }
}
