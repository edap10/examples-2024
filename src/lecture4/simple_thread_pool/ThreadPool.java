package lecture4.simple_thread_pool;

public class ThreadPool {

    private final Thread[] threads;
    private final TaskQueue q = new TaskQueue();
    public ThreadPool(int n) {
        threads = new Thread[n];
        for (int i = 0; i < n; i++) {
            threads[i] = new Thread(() -> {
                while (true) {
                    try {
                        Runnable task = q.awaitWork();
                        task.run();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        throw new Error(e);
                    }
                }
            });
        }
    }

    public void start() {
        for (Thread t : threads) {
            t.start();
        }
    }
    
    public void submit(Runnable r) {
        q.submit(r);
    }
}
