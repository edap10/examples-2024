package lecture4.simple_thread_pool;

import java.util.ArrayList;
import java.util.List;

public class TaskQueue {

    private final List<Runnable> tasks = new ArrayList<>();

    public synchronized void submit(Runnable r) {
        tasks.addLast(r);
        notifyAll();
    }

    public synchronized Runnable awaitWork() throws InterruptedException {
        while (tasks.isEmpty()) {
            wait();
        }
        return tasks.removeFirst();
    }
}
