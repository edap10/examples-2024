package lecture4;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class GuiDemo {

    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());
        SwingUtilities.invokeLater(() -> {
            System.out.println(Thread.currentThread().getName());
            JFrame f = new JFrame("test");
            JButton b = new JButton("click me!");
            b.addActionListener(e -> {
                System.out.println(Thread.currentThread().getName());
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            });
            f.add(b);
            f.pack();
            f.setVisible(true);
        });
    }

}
