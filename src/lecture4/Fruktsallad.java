package lecture4;

public class Fruktsallad {

    private static void hello(String s) {
        String name = Thread.currentThread().getName();
        while (true) {
            System.out.println(name + ": " + s);
        }
    }

    public static void main(String[] args) {
        Thread.startVirtualThread(() -> hello("apelsin"));
        hello("banan");
    }

}
