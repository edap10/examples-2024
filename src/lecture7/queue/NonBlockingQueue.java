package lecture7.queue;

import java.util.concurrent.atomic.AtomicReference;

// Slightly modified version (some bugs fixed, but
// several race conditions remain) of *flawed* example 
// found at:
//
// https://www.baeldung.com/lock-free-programming
//
// Beware: this does NOT work.

public class NonBlockingQueue<E> {
    private final AtomicReference<Node<E>> head = new AtomicReference<>(null);
    private final AtomicReference<Node<E>> tail = new AtomicReference<>(null);
    
    private static class Node<E> {
        private final E value;
        private volatile Node<E> next;

        public Node(E value) {
            this.value = value;
        }
    }

    public void add(E element) {
        Node<E> oldTail, newTail = new Node<>(element);
        do {
            oldTail = tail.get();
        } while(!tail.compareAndSet(oldTail, newTail));

        if(oldTail != null) {
            oldTail.next = newTail;
        }

        // if this is the first element, update head
        head.compareAndSet(null, newTail);
    }
    
    public E get() {
        Node<E> oldHead, newHead;
        do {
            oldHead = head.get();
            if(oldHead == null) {
                return null;
            }
            newHead = oldHead.next;
        } while(!head.compareAndSet(oldHead, newHead));
        
        // if this was the last element, update tail
        tail.compareAndSet(oldHead, null);

        return oldHead.value;
    }
}
