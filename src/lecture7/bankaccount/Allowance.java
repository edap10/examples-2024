package lecture7.bankaccount;

public class Allowance {

    public static void csn(BankAccount acc) {
        for (int i = 0; i < 100000; i++) {
            acc.deposit(10);
        }
    }

    public static void student(BankAccount acc) {
        for (int i = 0; i < 100000; i++) {
            acc.withdraw(10);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        BankAccount acc = new BankAccount();

        Thread t1 = new Thread(() -> csn(acc));
        Thread t2 = new Thread(() -> student(acc));

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println("pengar kvar = " + acc.getBalance());
    }
}
