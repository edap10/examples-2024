package lecture7.stack;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(OrderAnnotation.class)
class StackTest {
    private static final int NBR_VALUES = 1_000_000;

    private static boolean stackOk(int nbrThreads) throws InterruptedException {

        Stack<Integer> stack = new Stack<>();

        CyclicBarrier barrier = new CyclicBarrier(nbrThreads);
        CountDownLatch done = new CountDownLatch(nbrThreads);

        for (int i = 0; i < nbrThreads; i++) {
            new Thread(() -> {
                try {
                    barrier.await();

                    for (int k = 0; k < NBR_VALUES; k++) {
                        stack.push(5);
                        stack.pop();
                    }

                    done.countDown();
                } catch (InterruptedException | BrokenBarrierException unexpected) {
                    throw new Error(unexpected);
                }
            }).start();
        }

        done.await();

        // stack should be empty now
        return stack.isEmpty();
    }

    @Test
    @Order(1)
    void singleThreaded() throws InterruptedException {
        assertTrue(stackOk(1));
    }

    @Test
    @Order(2)
    void concurrent() throws InterruptedException {
        assertTrue(stackOk(8));
    }
}
