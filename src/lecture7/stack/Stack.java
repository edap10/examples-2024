package lecture7.stack;

import java.util.concurrent.atomic.AtomicReference;

public class Stack<E> {

    private final AtomicReference<Node<E>> top = new AtomicReference<>();

    public void push(E item) {
        Node<E> newTop = new Node<>(item);
        Node<E> oldTop;
        do {
            oldTop = top.get();
            newTop.next = oldTop;
        } while (!top.compareAndSet(oldTop, newTop));
    }

    public E pop() {
        Node<E> oldTop, newTop;
        do {
            oldTop = top.get();
            if (oldTop == null) {
                return null;
            }
            newTop = oldTop.next;
        } while (!top.compareAndSet(oldTop, newTop));
        return oldTop.item;
    }

    public boolean isEmpty() {
        return (top.get() == null);
    }

    // -----------------------------------------------------------------------

    private static class Node<E> {
        private final E item;
        private Node<E> next = null;

        public Node(E item) {
            this.item = item;
        }
    }
}
