package lecture7.nbr_cores;

public class CheckCoreCount {

    public static void main(String[] args) {

        // The number of 'processors' (or cores) obtained here
        // is the number of threads that can run simultaneously
        // in the hardware. This includes all types of cores,
        // including performance/efficiency cores or hyperthreading.
        //
        // For example, the following configurations would all
        // be reported as four cores:
        //
        // - A CPU with four (single-thread) cores
        //
        // - A CPU with two cores, each capable of running
        //   two threads simultaneously (hyperthreading)
        //
        // - A CPU with two performance cores
        //   and two efficiency cores

        int n = Runtime.getRuntime().availableProcessors();
        System.out.println("number of processors: " + n);
    }
}
