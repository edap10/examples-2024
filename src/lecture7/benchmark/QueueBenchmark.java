package lecture7.benchmark;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

public class QueueBenchmark {

    private static final int MAX_THREADS = 80;
    private static final int THREAD_STEP = 2;
    private static final int NBR_ITERATIONS = 1_000_000;

    private static long benchmark(int nbrThreads, int queueSize, Queue<Integer> q) throws InterruptedException {

        // populate queue with some test data
        for (int i = 0; i < queueSize; i++) {
            q.add(Integer.MIN_VALUE);
        }

        System.out.println("benchmarking for " + nbrThreads + "  (" + q.getClass().getSimpleName() + ")");

        // run threads to add/remove elements
        AtomicLong duration = new AtomicLong(0);
        Set<Thread> threads = new HashSet<>();
        CyclicBarrier barrier = new CyclicBarrier(nbrThreads);
        for (int i = 0; i < nbrThreads; i++) {
            Integer testdata = Integer.valueOf(i);
            Thread t = new Thread(() -> {
                try {
                    barrier.await();
                    long t0 = System.currentTimeMillis();
                    for (int k = 0; k < NBR_ITERATIONS; k++) {
                        q.add(testdata); // O(1)
                        q.remove(testdata); // O(n)
                    }
                    long dt = System.currentTimeMillis() - t0;
                    duration.addAndGet(dt);
                } catch (Throwable e) {
                    e.printStackTrace(System.err);
                }
            });
            t.start();
            threads.add(t);
        }

        for (Thread t : threads) {
            t.join();
        }

        int remaining = q.size();
        if (remaining != queueSize) {
            throw new Error("incorrect number of elements remaining: " + remaining + ", expected " + queueSize);
        }

        return duration.get() / nbrThreads;
    }

    public static void runBenchmark(int queueSize) throws InterruptedException {
        System.out.println("### benchmark for queue size " + queueSize + ":");

        List<String> lockfreeTimes = new ArrayList<>();
        for (int i = MAX_THREADS; i >= 1; i -= THREAD_STEP) {
            long t = benchmark(i, queueSize, new ConcurrentLinkedQueue<>());
            lockfreeTimes.add(0, String.format("%5d", t));
        }

        List<String> lockTimes = new ArrayList<>();
        for (int i = MAX_THREADS; i >= 1; i -= THREAD_STEP) {
            long t = benchmark(i, queueSize, new LinkedBlockingQueue<>());
            lockTimes.add(0, String.format("%5d", t));
        }

        System.out.print("             \"#threads\";");
        for (int i = THREAD_STEP; i < MAX_THREADS; i += THREAD_STEP) {
            System.out.printf("%5d;", i);
        }
        System.out.printf("%5d\n", MAX_THREADS);

        System.out.println("\"ConcurrentLinkedQueue\";" + String.join(";", lockfreeTimes));
        System.out.println("  \"LinkedBlockingQueue\";" + String.join(";", lockTimes));
    }

    public static void main(String[] args) throws InterruptedException {
        // benchmark in the lecture slides where obtained as follows
        // (on AMD EPYC 7713P, 64 cores, SMT disabled, CPU frequency fixed to 2GHz)
        runBenchmark(320);
        runBenchmark(600);
    }
}
