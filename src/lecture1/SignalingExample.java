package lecture1;

import java.util.concurrent.Semaphore;

public class SignalingExample {
    public static void doStuff(String name, Semaphore a, Semaphore b) {
        try {
            a.acquire();                      //
            for (int t = 0; t < 10000; t++) { //
                System.out.println(name);     //
            }                                 //
            b.release();                      //
        } catch (InterruptedException e) {
            throw new Error(e);
        }
    }

    public static void main(String[] args) {
        Semaphore s1 = new Semaphore(1);
        Semaphore s2 = new Semaphore(0);
        Thread t1 = new Thread(() -> doStuff("One", s1, s2));
        Thread t2 = new Thread(() -> doStuff("Two", s2, s1));
        t1.start();
        t2.start();
    }
}