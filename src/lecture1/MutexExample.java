package lecture1;

import java.util.concurrent.Semaphore;

public class MutexExample {
    public static void doStuff(String name, Semaphore s) {
        try {
            s.acquire();                      //
            for (int t = 0; t < 10000; t++) { //
                System.out.println(name);     //
            }                                 //
            s.release();                      //
        } catch (InterruptedException e) {
            throw new Error(e);
        }
    }

    public static void main(String[] args) {
        Semaphore s = new Semaphore(1);
        Thread t1 = new Thread(() -> doStuff("One", s));
        Thread t2 = new Thread(() -> doStuff("Two", s));
        t1.start();
        t2.start();
    }
}