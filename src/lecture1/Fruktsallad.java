package lecture1;

public class Fruktsallad {

    private static void hello(String s) {
        String name = Thread.currentThread().getName();
        while (true) {
            System.out.println(name + ": " + s);
        }
    }

    public static void main(String[] args) {
        new Thread(() -> hello("apelsin")).start();
        hello("banan");
    }

}
