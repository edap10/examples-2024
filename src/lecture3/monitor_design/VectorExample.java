package lecture3.monitor_design;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

public class VectorExample {

    public static void main(String[] args) {

        List<String> list = new Vector<String>();

        Thread t1 = new Thread(() -> {
            while (true) {
                var x = list.remove("cowabunga");
                System.out.println(x);
                doOtherStuff();
            }
        });

        Thread t2 = new Thread(() -> {
            while (true) {
                addRandomWordsTo(list);
            }
        });

        t1.start();
        t2.start();
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    private static void doOtherStuff() {
        try {
            Thread.sleep(2);
        } catch (InterruptedException e) {
            throw new Error(e);
        }
    }

    private static void addRandomWordsTo(List<String> list) {
        try {
            Thread.sleep(2);
            int idx = ThreadLocalRandom.current().nextInt(WORDS.length);
            list.add(0, WORDS[idx]);
        } catch (InterruptedException e) {
            throw new Error(e);
        }
    }

    static final String[] WORDS = { "cowabunga", "bus", "goat", "tree", "code", "oven", };
}
