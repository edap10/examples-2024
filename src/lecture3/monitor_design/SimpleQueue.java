package lecture3.monitor_design;

import java.util.LinkedList;

public class SimpleQueue<E> {
    private LinkedList<E> q = new LinkedList<>();

    /** Add e to end of queue. */
    public synchronized void put(E e) {
        q.addLast(e);
        notifyAll();
    }

    /** Block until an element is available. */
    public synchronized E waitUntilAvailableAndGet() throws InterruptedException {
        while (q.isEmpty()) {
            wait();
        }

        return q.removeFirst();
    }

    // =======================================================================

    public static void main(String[] args) {
        SimpleQueue<String> queue = new SimpleQueue<>();

        // 3 consumers
        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                try {
                    while (true) {
                        String s = queue.waitUntilAvailableAndGet();
                        System.out.println(s);
                    }
                } catch (InterruptedException unexpected) {
                    throw new Error(unexpected);
                }
            }).start();
        }

        // producer
        new Thread(() -> {
            try {
                while (true) {
                    queue.put("stuff");
                    Thread.sleep(10);
                }
            } catch (InterruptedException unexpected) {
                throw new Error(unexpected);
            }
        }).start();
    }
}
