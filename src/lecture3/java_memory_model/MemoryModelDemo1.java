package lecture3.java_memory_model;

public class MemoryModelDemo1 {
    private volatile int x = 0;

    public void setX() {
        x = 7;
        System.out.println("X was changed");
    }

    public void waitForX() {
        System.out.println("waiting for X to change");
        while (x == 0) {
            /* spin */
        }
        System.out.println("change detected: x = " + x);
    }

    // =======================================================================

    public static void main(String[] args) throws InterruptedException {
        MemoryModelDemo1 demo = new MemoryModelDemo1();
        Thread t1 = new Thread(() -> demo.waitForX());
        t1.start();

        // T2 is main thread
        Thread.sleep(1000);
        demo.setX();
    }
}
