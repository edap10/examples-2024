package lecture3.java_memory_model;

public class MemoryModelDemo2 {

    private double a;
    private volatile int handshake;

    private void runDemo() {
        ProgressWindow w = new ProgressWindow();
        w.startTimer();

        // thread that updates a
        new Thread(() -> {
            while (true) {
                a += 3e-8;
                handshake = 1;
                w.update1(a);
            }
        }).start();

        // thread that reads a
        new Thread(() -> {
            while (true) {
                if (handshake < 0) {
                    System.out.println("huh?");
                }
                w.update2(a);
            }
        }).start();
    }

    public static void main(String[] args) {
        MemoryModelDemo2 d = new MemoryModelDemo2();
        d.runDemo();
    }
}
