package lecture3.java_memory_model;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 * A window for displaying updates from two threads. Used for demonstrating the
 * Java Memory Model.
 * 
 * EDAP10 Concurrent Programming, Lund University (Patrik Persson)
 */
@SuppressWarnings("serial")
public class ProgressWindow {

    private final Frame frame;

    public ProgressWindow() {
        try {
            CompletableFuture<Frame> f = new CompletableFuture<>();
            SwingUtilities.invokeLater(() -> f.complete(new Frame()));
            frame = f.get();
        } catch (InterruptedException | ExecutionException unexpected) {
            throw new Error(unexpected);
        }
    }

    public void startTimer() {
        new Timer(16, e -> {
            frame.display1.repaint();
            frame.display2.repaint();
        }).start();
    }

    public void update1(double alpha) {
        frame.display1.alpha = alpha;
    }

    public void update2(double alpha) {
        frame.display2.alpha = alpha;
    }

    private class Frame extends JFrame {

        private final ProgressDisplay display1 = new ProgressDisplay(1),
                                      display2 = new ProgressDisplay(2);

        public Frame() {
            super("ProgressWindow");
            setLayout(new FlowLayout());
            add(display1);
            add(display2);
            pack();
            setLocationRelativeTo(null);
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setVisible(true);
        }
    }

    private static final Dimension SIZE = new Dimension(150, 150);

    private class ProgressDisplay extends JPanel {
        private final String label;

        // NOTE: the attribute 'alpha' should really be volatile.
        //
        // However, declaring it volatile would make the demo fail (!)
        // because the assignment to alpha would make previous writes visible.
        //
        // When alpha is not volatile, we will observe the value written
        // before the last write to the demo application's volatile attribute.
        // This means that the value displayed will actually not be the latest
        // value written, but the _previous_ one. This is OK for the demo.

        private double alpha;

        public ProgressDisplay(int k) {
            super(null);
            setPreferredSize(SIZE);
            setFont(new Font(Font.SANS_SERIF, Font.BOLD, 100));

            this.label = Integer.toString(k);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            Graphics2D g2d = (Graphics2D) g;
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            Graphics2D rotated = (Graphics2D) g2d.create();
            rotated.rotate(alpha, SIZE.width / 2, SIZE.height / 2);
            rotated.setColor(Color.LIGHT_GRAY);
            rotated.fillOval(0, 0, SIZE.width, SIZE.height);
            rotated.setColor(Color.RED);
            rotated.fillArc(0, 0, SIZE.width, SIZE.height, 0, 90);
            rotated.setColor(Color.GREEN);
            rotated.fillArc(0, 0, SIZE.width, SIZE.height, 180, 90);
            rotated.dispose();

            FontMetrics m = g2d.getFontMetrics();
            float x = 0.5f * (SIZE.width - m.stringWidth(label));
            float y = 0.5f * (SIZE.height + m.getAscent() - m.getDescent() - m.getLeading());

            g2d.drawString(label, x, y);
        }
    }
}
