package lecture2.example_from_film;

public class Allowance {

    public static void csn(BankAccount acc) {
        for (int i = 0; i < 10000; i++) {
            acc.deposit(1);
        }
    }

    public static void student(BankAccount acc) {
        try {
            for (int i = 0; i < 10000; i++) {
                acc.withdraw(1);
            }
        } catch (InterruptedException e) {
            throw new Error(e);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        BankAccount acc = new BankAccount();

        Thread t1 = new Thread(() -> csn(acc));
        Thread t2 = new Thread(() -> student(acc));

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println("pengar kvar = " + acc.getBalance());
    }
}
