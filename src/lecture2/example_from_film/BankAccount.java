package lecture2.example_from_film;

public class BankAccount {
    private int balance = 0;

    public synchronized void deposit(int amount) {
        balance += amount;
        notify();
    }

    public synchronized void withdraw(int amount) throws InterruptedException {
        while (balance < amount) {
            wait();
        }
        balance -= amount;
        if (balance < 0) {
            System.out.println("pank!");
        }
    }
    
    public synchronized int getBalance() {
        int b = balance;
        return b;
    }
}
