package lecture6.messaging_deadlock;

// import actor.ActorThread;
import actor_with_bounded_queue.ActorThread;

public class PingPong {

    final Ping ping = new Ping();
    final Pong pong = new Pong();

    class Ping extends ActorThread<Integer> {
        @Override
        public void run() {
            try {
                while (true) {
                    int m = receive();
                    System.out.println("ping: " + m);
                    pong.send(m + 1);
                }
            } catch (InterruptedException e) {
                // Exception not expected:
                // alert the user
                throw new Error(e);
            }
        }
    }

    class Pong extends ActorThread<Integer> {
        @Override
        public void run() {
            try {
                while (true) {
                    int m = receive();
                    System.out.println("pong: " + m);
                    ping.send(m + 1);
                    ping.send(m + 1);
                }
            } catch (InterruptedException e) {
                // Exception not expected:
                // alert the user
                throw new Error(e);
            }
        }
    }

    public void begin() {
        ping.start();
        pong.start();
        ping.send(1);
    }

    public static void main(String[] args) {
        new PingPong().begin();
    }
}
