package lecture6.alien_method;

public class BankExample {

    private static Bank bank = new Bank();
    private static BankAccount account = new BankAccount();

    private static String accountFormat(int balance) {
        return balance + " kr (ränta: " + bank.getInterest() + ")";
    }

    public static void main(String[] args) {
        bank.addAccount(account);

        bank.setInterest(0.03);

        new Thread(() -> {
            try {
                while (true) {
                    Thread.sleep(100);
                    bank.addInterest();
                }
            } catch (InterruptedException unexpected) {
                throw new Error(unexpected);
            }
        }).start();

        new Thread(() -> {
            while (true) {
                String s = account.balanceAsString(balance -> accountFormat(balance));
                System.out.println(s);
            }
        }).start();
    }
}
