package lecture6.alien_method;

import java.util.function.Function;

public class BankAccount {

    //
    // RESOURCE ORDERING:
    //
    // Bank can call BankAccount with lock held,
    // but not the other way around.
    //

    private int balance = 0;

    public synchronized void deposit(int amount) {
        balance += amount;
    }

    public synchronized void withdraw(int amount) {
        balance -= amount;
    }

    public synchronized int balance() {
        return balance;
    }

    public synchronized void addInterest(double interest) {
        balance += balance * interest;
    }

    public synchronized String balanceAsString(Function<Integer, String> formatter) {
        return formatter.apply(balance);
    }
}
