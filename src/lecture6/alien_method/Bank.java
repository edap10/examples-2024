package lecture6.alien_method;

import java.util.ArrayList;
import java.util.List;

public class Bank {

    //
    // RESOURCE ORDERING:
    //
    // Bank can call BankAccount with lock held,
    // but not the other way around.
    //

    private List<BankAccount> accounts = new ArrayList<>();
    private double interest;

    public synchronized void addAccount(BankAccount a) {
        accounts.add(a);
    }

    public synchronized void addInterest() {
        for (BankAccount account : accounts) {
            account.addInterest(interest);
        }
    }

    public synchronized void setInterest(double newInterest) {
        interest = newInterest;
    }

    public synchronized double getInterest() {
        return interest;
    }
}
