package lecture6.lock_deadlock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockDeadlock {
    
    public static void main(String[] args) {

        final Lock a = new ReentrantLock(),
                   b = new ReentrantLock(),
                   c = new ReentrantLock(),
                   d = new ReentrantLock();

        new Thread(() -> {
            while (true) {
                System.out.println("T1 taking locks...");
                a.lock();
                b.lock();
                c.lock();  // <---
                System.out.println("T1 holds a, b, c");
                c.unlock();
                b.unlock();
                a.unlock();
            }
        }, "T1").start();

        new Thread(() -> {
            while (true) {
                System.out.println("T2 taking locks...");
                b.lock();  // <---
                c.lock();
                d.lock();
                System.out.println("T2 holds d, c, b");
                b.unlock();
                c.unlock();
                d.unlock();
            }
        }, "T2").start();
    }

}
