package lecture6.monitor_deadlock;

public class MonitorDeadlock {

    private final A a = new A();
    private final B b = new B();
    private final C c = new C();

    public class A {
        public synchronized void beepHonk() {
            System.out.println("beep");
            b.honk();
        }
        
        public synchronized void zapClick() {
            c.zap();
            System.out.println("click");
        }
    }

    public class B {
        public synchronized void honk() {
            System.out.println("honk");
        }
        
        public synchronized void ringRing() {
            c.ring();
            c.ring();
        }
    }

    public class C {
        public synchronized void zap() {
            System.out.println("zap");
        }
        
        public synchronized void ring() {
            System.out.println("ring");
        }
    }

    // =======================================================================

    public void t1run() {
        while (true) {
            a.beepHonk();
        }
    }

    public void t2run() {
        while (true) {
            b.ringRing();
        }
    }

    public void t3run() {
        while (true) {
            a.zapClick();
        }
    }

    public void danger() {
        Thread t1 = new Thread(() -> t1run());
        Thread t2 = new Thread(() -> t2run());
        Thread t3 = new Thread(() -> t3run());
        t1.start();
        t2.start();
        t3.start();
    }

    public static void main(String[] args) {
        MonitorDeadlock m = new MonitorDeadlock();
        m.danger();
        System.out.println("main running");
    }
}

